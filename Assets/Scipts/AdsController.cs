﻿//Author: Rok Kos <rocki5555@gmail.com>
//File: AdsController.cs
//File path: /D/Documents/Unity/GimVicVodic/Assets/Scipts/AdsController.cs
//Date: 27.03.2016
//Description: Script to control ads and to show to users

using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class AdsController : MonoBehaviour {

	private string typeOfAds = "video";

	/// <summary>
	/// Funtion that could be called from outside to show add
	/// </summary>
	public void ShowAd()
	{
		//if we are in editor simulate delay and stop game time
		#if UNITY_EDITOR
			StartCoroutine(WaitForAd ());
		#endif
		
		//Starts corutine to show ad when is ready
		StartCoroutine(ShowAdWhenReady());
		
		//if (Advertisement.isReady ())
		//	Advertisement.Show (typeOfAds, options);
	}

	/// <summary>
	/// Funtion that handles result of the ad and retyrn feadback to user
	/// </summary>
	void AdCallbackhandler (ShowResult result)
	{
		switch(result)
		{
		case ShowResult.Finished:
			Debug.Log ("Ad Finished. Rewarding player...");
			break;
		case ShowResult.Skipped:
			Debug.Log ("Ad skipped. Son, I am dissapointed in you");
			break;
		case ShowResult.Failed:
			Debug.Log("I swear this has never happened to me before");
			break;
		}
	}
	/// <summary>
	/// Funtion that simulates ad for editor
	/// </summary>
	IEnumerator WaitForAd()
	{
		float currentTimeScale = Time.timeScale;
		Time.timeScale = 0f;
		yield return null;

		while (Advertisement.isShowing)
			yield return null;

		Time.timeScale = currentTimeScale;
	}

	/// <summary>
	/// Function that show ads when its ready
	/// </summary>
	IEnumerator ShowAdWhenReady(){

	 	//asign funtion that deals with information after the add had finished
		ShowOptions options = new ShowOptions ();
		options.resultCallback = AdCallbackhandler;

		//if isnt ready wait
        while (!Advertisement.IsReady())
            yield return null;
        //show add
        Advertisement.Show (typeOfAds, options);
    }
}