﻿//Author: Rok Kos
//Date: 05.12.2015
//Description: Script for controling UI

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System;
//using UnityEngine.WSA; //building toast

public class UIController : MonoBehaviour {

	Dictionary<string, GameObject> AllMenus = new Dictionary<string, GameObject> (); //dictionary of all menus
	private string FirstMenu = "homeMenu"; //string represents first menu that will show infront of user
	private string regexSearchPatternPanel = "Panel"; //string by which are searched menus
	private List<string> previousMenu = new List<string>(){"homeMenu"}; //string that represents previous menu
	private List<string> RandomFacts = new List<string>(){
		"Ali ste vedeli, da je imela gimnazija Vič svojo številko matematičnega časopisa Matematik na kolesu (pazi volka na drevesu).",
		"V gimnazijo Vič hodijo dijaki, že od leta 1929.",
		"Leta 1935 šola dobi naziv I. državna mešano meščanska šola Josipa Jurčiča v Ljubljani.",
		"Že leta 1980 so sestavili Program dela z mladino v prostem času, ki je vseboval veliko izvenšolskih krožkov za dijake.",
		"Od leta 16. 12. 2001 je Gimnazija Vič tudi UNESCO šola, kar omogoča sodelovanje z vsemi pridruženimi članicami po vsem svetu."
	}; //list that show on loading screen to intertain user
	private IzberiDestinacijo izberiDestinacijo;
	[SerializeField] GameObject zacetnaPozicijaText; //for showing user what he picked
	[SerializeField] GameObject koncnaPozicijaText; //for showing user what he picked
	private string zacetekDisplayText = "Zacetna pozicija : "; //for displaying text
	private string koncnaDisplayText = "Koncna pozicija : "; //for displaying text
	[SerializeField] ReadLocation readLocation;
	[SerializeField] GameObject creditsText; //credist text to move up text
	[SerializeField] Text loadingSceneText; //Object for changing text while loading new scene
	[SerializeField] GameObject loadingSceneImage; //Object for rotating image while loading new scene
	[SerializeField] Text loadingPercentageText; //Object for changing text while loading new scene
	[SerializeField] Image loadingPercentageImage; //Object for changing fill amount while loading new scene
	private int directionOfFilling = 1; //in which way the bar will fill
	private int stageOfFilling = 0;
	//private Image.fillOrigin fillOrigin = Image.fillOrigin.Left; //from which origin will bar fill
	
	void Awake () {
		//Set scren in portrait mode 
		Screen.orientation = ScreenOrientation.Portrait;

		//Geting all panels by searching with regex expresion
		GameObject[] AllGameObjects = GameObject.FindGameObjectsWithTag(regexSearchPatternPanel);
		for (int i = 0; i< AllGameObjects.Length; ++i) {
			GameObject selectedGameObject = AllGameObjects[i];
			//Debug.Log(selectedGameObject.name);
			//Assining gameobject to name in dictionary
			AllMenus[selectedGameObject.name] = selectedGameObject;
		}
		//enable hometMenu
		enableOneMenu (FirstMenu);
		izberiDestinacijo = transform.GetComponent<IzberiDestinacijo>();
		PlayerPrefs.SetInt("WhereSave", 0);
		//set desciption of showing text
		PlayerPrefs.SetString("zacText",new String('_', 25));//25 times repeat character _
		PlayerPrefs.SetString("konText",new String('_', 25));
		PlayerPrefs.SetInt("begin", -1);
		PlayerPrefs.SetInt("target", -1);
		//set readLocation script for use
		//and deactivate so that it save the RAM
		//readLocation = GetComponent<ReadLocation>();
		readLocation.enabled = false;
	}
	/// <summary>
	/// Checks if back button is clicked and then goes to previous Menu
	/// </summary>	
	void Update(){
		if(Input.GetKeyDown(KeyCode.Escape)){
			int lastElement = previousMenu.Count - 1;
			//Debug.Log(lastElement.ToString());\
			//Debug.Log(previousMenu[lastElement-1]);
			if(lastElement > 0){
				//erase buttons so that you dont get duplicated
				if(previousMenu[lastElement-1] != "selectionMenu"){
					izberiDestinacijo.EraseButtons();
					//Debug.Log(previousMenu[lastElement-1]);
				}
				enableOneMenu(previousMenu[lastElement-1]); //get last menu
				previousMenu.RemoveAt(lastElement); //remove it so we dont go to that menu again

			}
			else{
				//if its in main menu and there is no previous menus and escape(back in mobile) is pressed
				//then exit the aplication
				Application.Quit();
			}
		}


	}

	/// <summary>
	/// Function that enables one menu and disables other ones.
	/// Ko urejas to kodo pazi na imenovanje panelov oz. menujov in gumbov, ki kazejo na ta menu.
	/// Morata biti istega imena razlikujeta se lahko samo na koncu z "Button" in "Menu"
	/// Be very careful when naming panels(menus) and buttons which show on menu.
	/// They must have same name and only differ in appendix with "Button" and "Menu"
	/// </summary>
	/// <param name="nameOfMenu">Name of menu.</param>
	void enableOneMenu(string nameOfMenu){
		//Going thru every item in dictionary
		foreach(KeyValuePair<string, GameObject> menu in AllMenus){
			//checks name
			if(menu.Key == nameOfMenu){
				//and set active
				menu.Value.SetActive(true);
				//special cases
				if(menu.Key == "selectionMenu"){
					//calls script wiht right parameters
					transform.GetComponent<IzberiDestinacijo>().PopulateDropDownMenu("", 1);
				}
				if(menu.Key == "navigationMenu"){
					if(PlayerPrefs.GetInt("WhereSave",0) == 0){
				  		zacetnaPozicijaText.GetComponentInChildren<Text>().text = zacetekDisplayText + PlayerPrefs.GetString("zacText", "");
				  		koncnaPozicijaText.GetComponentInChildren<Text>().text = koncnaDisplayText + PlayerPrefs.GetString("konText", "");
				  	}

					if(PlayerPrefs.GetInt("WhereSave",0) == 1){
				  		zacetnaPozicijaText.GetComponentInChildren<Text>().text = zacetekDisplayText + PlayerPrefs.GetString("zacText", "");
				  	}
				  	if(PlayerPrefs.GetInt("WhereSave",0) == 2){
				  		koncnaPozicijaText.GetComponentInChildren<Text>().text = koncnaDisplayText + PlayerPrefs.GetString("konText", "");
				  	}
				}
				if(menu.Key == "kje_semMenu"){
					//enable reading nfc when user gets in that menu
					readLocation.enabled = true;
				}
				//Show credits by moving text up 
				if(menu.Key == "navodilaMenu"){
					//Debug.Log("here");
					StartCoroutine(moveTextUp());
				}

			}else{
				//or unactive
				menu.Value.SetActive(false);
				//disable reading nfc when user exits menu so that we dont use to much RAM
				readLocation.enabled = false;
			}
		}
	}
	
	//Function that changes selected menu to navigation menu
	public void gotoMenu(Button button){
		string regexButton = "_Button";
		//Remove substing from string in this case "_Button"
		string menuName = button.name.Remove(button.name.IndexOf(regexButton), regexButton.Length);
		//dont go to selection
		//beacuse you dont want to pick twice
		if(menuName != "selection"){
			previousMenu.Add(menuName + "Menu"); //add to previousMenu array so we know from where we came
		}
		//printPreviousMenu();
		enableOneMenu(menuName + "Menu");
	}
	/// <summary>
	/// Goes to the main level when clicked on button. Boolean Mode tells us in which mode whil player
	/// go: in navigation or free roam. 
	/// Warning only one box in Unity decided in which mode goes
	/// </summary>
	/// <param name="nameOfMenu">Name of menu.</param>
	public void gotoMainLevel(bool Mode){
		//Goes to navigation
		if(Mode){
			PlayerPrefs.SetInt("mode",1);
			//if postions are okey then goes to main
			if(checkPositions()){
				ShowLoadingScreen();
				 StartCoroutine(LoadNewScene("main"));
			}
			//else makes toast and show to user
			else{
				
			}
		}
		//Goes to 
		else{
			PlayerPrefs.SetInt("mode",-1);
			ShowLoadingScreen();
			 StartCoroutine(LoadNewScene("main"));
		}
	}
	/// <summary>
	/// Goes to the submenu when clicked on button. Int Mode tells us where will varianble save after 
	/// user pick ucilnico.
	/// Int 1: Zacetna pozicija
	/// Int 2: Koncna pozicija
	/// </summary>
	public void gotoSubMenu(int Mode){
		//variable where will be the result saved
		PlayerPrefs.SetInt("WhereSave", Mode);
		//create a temporary Button just to send forward Menu name
		//and that is done by adding component button to game object
		//because UI.Button itself cannot be object
		//and when we create object we also give him name
		GameObject tempButton = new GameObject();
		tempButton.AddComponent<Button>().name = "selection_Button";
		//and here we dont pass all objet but just button component
		gotoMenu(tempButton.GetComponent<Button>());
		//destroying Button
		Destroy(tempButton,0);
		
	}
	/// <summary>
	/// Goes to navigation menu
	/// </summary>
	public void saveProstor(){
		//just goes to menu without saving last postion so that it doesnt come to duplicates in previousMenu array
		enableOneMenu("navigationMenu");
	}

	/// <summary>
	/// Check if position are okey and returns boolean
	/// </summary>
	bool checkPositions(){
		int tempBegin = PlayerPrefs.GetInt("begin",-1);
		int tempTarget = PlayerPrefs.GetInt("target",-1);
		Debug.Log("b " + tempBegin.ToString() + " t " + tempTarget.ToString());
		//checks if not empty and if not same
		if(tempBegin != tempTarget && tempTarget != -1 && tempBegin != -1){
			return true;
		}
		//if it true then function will not execute this part
		return false;

	}

	void printPreviousMenu(){
		Debug.Log("---------------------");
		for (int i=0; i<previousMenu.Count; ++i) {
			Debug.Log(previousMenu[i]);
		}

	}

	/// <summary>
	/// Funktion that moves text up in the screen
	/// </summary>
	IEnumerator moveTextUp(){
		float ScreenHeight = Screen.height;//get scren height
		float moveRate = 5.0f;//tells how fast text is moving
		//goes to half of the screnn size
		while(creditsText.transform.position.y < ScreenHeight / 2.0f){
			//move text up by move rate
			//Debug.Log(creditsText.transform.position);
			creditsText.transform.position = new Vector3(creditsText.transform.position.x, creditsText.transform.position.y + moveRate, creditsText.transform.position.z);
			//this line keep proces runing
			yield return null;
		}
		//ends proces
		yield return new WaitForSeconds(0.01f);
	}

	/// <summary>
	/// Loads new scene in background while showing loading screen
	/// </summary>
	IEnumerator LoadNewScene(string nameOfScene) {

		


   	    // Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
        AsyncOperation async = Application.LoadLevelAsync(nameOfScene);

        // While the asynchronous operation to load the new scene is not yet complete, continue waiting until it's done.
        while (!async.isDone) {
        	//rotate image to represent that content is loading
        	loadingPercentageText.text = (async.progress * 100.0f).ToString() + "%";
        	LoadingBarProgress(0.01f);
        	loadingSceneImage.transform.RotateAround(loadingSceneImage.transform.position,  Vector3.forward, 20);
        	//Debug.Log(async.progress);
            yield return null;
        }

    }
    /// <summary>
    /// Function that shows loading screen and chooses fun fact
    /// </summary>
    void ShowLoadingScreen(){
    	//enables loading screen
    	enableOneMenu("loadingScreenMenu");
    	//namespace UnityEngine is because system namespace also uses random funtion
    	int idFact = UnityEngine.Random.Range(0,RandomFacts.Count); // pick random fact
    	loadingSceneText.text = RandomFacts[idFact]; //display random fact


    }

    /// <summary>
    /// Function that shows loading screen and chooses fun fact
    /// Explanation of swich statement:
    /// Case 0: Bar got to the right end full so we change direction and fill origin
    /// Case 1: Bar got to the right end empty so we change only direction 
    /// Case 2: Bar got to the left end full so we change direction and fill origin
    /// Case 1: Bar got to the left end empty so we change only direction and send it to the first stage 
    /// </summary>
    void LoadingBarProgress(float amountToChange){
    	if(loadingPercentageImage.fillAmount == 0.0f || loadingPercentageImage.fillAmount == 1.0f){

			switch (stageOfFilling) {
				case 0:
					// cast fill origin into int so that it changes from where it fills
					loadingPercentageImage.fillOrigin = (int) Image.OriginHorizontal.Right; 
					directionOfFilling *= -1;
					stageOfFilling++;
					break;
				case 1:
					directionOfFilling *= -1;
					stageOfFilling++;
					break;
				case 2:
					loadingPercentageImage.fillOrigin = (int) Image.OriginHorizontal.Left;
					directionOfFilling *= -1;
					stageOfFilling++;
					break;
				case 3:
					directionOfFilling *= -1;
					//back to firs stage
					stageOfFilling = 0;
					break;
			}
		}
    	//loadingPercentageImage.fillOrigin = fillOrigin;
    	Debug.Log(loadingPercentageImage.fillAmount.ToString());
    	loadingPercentageImage.fillAmount += amountToChange * directionOfFilling;

    }
}