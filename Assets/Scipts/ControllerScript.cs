﻿//Author: Rok Kos
//Date: 05.12.2015
//Description: Script that controls whole game

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using GimVic;//where are all classroms defineds

public class ControllerScript : MonoBehaviour {

	[SerializeField]GameObject HUDMenu;
	[SerializeField]TestingScript _TestingScript; //for enabling and disabling(testing script)
	[SerializeField]GameObject _Joystick;//joystick
	bool game_On = true;
	public float HitrostIgre = 1.0f; //for declaring how fast shoudl game go
	[SerializeField]GameObject player; //serialize player
	public GameObject movingNavMesh;
	private NavMeshAgent agentFromPlayer;
	private List<Vector3> target = new List<Vector3>(); //targets where to go
	//statical positioning of entrance of school WARNING CHANGE WHEN CHANGES POSTION OF SCHOOL
	private Vector3 pozicijaVhoda = new Vector3(-21.046f, 0.58f, -2.06f);
	private int end_Target = 0;//where to go
	private int begin_Target = 0; //where to start
	private int setMode = -1; //in which mode starts game
	[SerializeField] AdsController adsController;

	void Awake () {
		//Set scren in landscape mode
		Screen.orientation = ScreenOrientation.LandscapeLeft;
		HUDMenu.SetActive (false);
		target = ProstoriSole.target; //initalize all the targets
		//getting in which mode is
		setMode = PlayerPrefs.GetInt("mode",1);
		//Navigation mode
		if(setMode == 1){
			_TestingScript.enabled = false;
			_Joystick.SetActive(false);
			Time.timeScale = HitrostIgre;
			begin_Target = PlayerPrefs.GetInt ("begin",0);
			//Debug.Log(begin_Target.ToString());
			//Instantiate player at starting location and runs set agent function
			movingNavMesh =  Instantiate(player, target[begin_Target], Quaternion.identity) as GameObject;
			movingNavMesh.name = "Clovek";
			movingNavMesh.GetComponent<Premikanje>().SetAgent();
			agentFromPlayer = movingNavMesh.GetComponent<NavMeshAgent>();

		}
		//Free roam mode
		else{
			_TestingScript.enabled = false;
			_Joystick.SetActive(true);
			Time.timeScale = HitrostIgre;
			movingNavMesh =  Instantiate(player, pozicijaVhoda, Quaternion.identity) as GameObject;
			movingNavMesh.name = "Clovek";
			movingNavMesh.transform.Rotate(0,90,0,Space.Self);
		}
	}

	void Update () {
		//Ce je igra ob kliku na gumb nazaj ustavljena potem jo zazeni drugace jo ustavi
		if (Input.GetKeyDown (KeyCode.Escape) && game_On) {
			App_Stop (); 

		} else if (Input.GetKeyDown (KeyCode.Escape) && !game_On) {
			App_Play();
		}

	}
	//Function that stops game and enables HUD menu
	public void App_Stop(){
		game_On = !game_On;
		
		//only stops agent if in navigation mode
		if(setMode == 1){agentFromPlayer.Stop();}
		HUDMenu.SetActive (true);
		//TODO: Visualno spremeni da bo bolj pocasi ustavilo
		Time.timeScale = 0;
	}
	//Function that resume game and disables HUD menu
	public void App_Play(){
		game_On = !game_On;
		
		//only resumes agent if in navigation mode
		if(setMode == 1){agentFromPlayer.Resume();}
		HUDMenu.SetActive (false);

		Time.timeScale = HitrostIgre;
	}

		//Function that increases agent speed
	public void increase_Agent_Speed(){
		HitrostIgre *= 1.5f;
	}

	//Function that decreases agent speed
	public void decrease_Agent_Speed(){
		HitrostIgre /= 1.5f;
	}

	/// <summary>
	/// Goes to main menu
	/// </summary>
	public void GoToMainMenu(){
		//show ad
		adsController.ShowAd();
		Application.LoadLevel("MainMenu");		
	}
}
