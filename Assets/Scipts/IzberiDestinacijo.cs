//Author: Rok Kos <rocki5555@gmail.com>
//File: IzberiDestinacijo.cs
//File path: /D/Documents/Unity/GimVicVodic/Assets/Scipts/IzberiDestinacijo.cs
//Date: 13.1.2016
//Description: Script which will lead user throught the proces of chosing classroms

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using GimVic;//where are all classroms defined

public class IzberiDestinacijo : MonoBehaviour {	

	//varialbe that is passed to navmesh for start and end location
	private int izbran_start_id = -1,
				izbran_konec_id = -1;
	private List<Sola.Ucilnica> ucilnice;//array of objects which represent all classroms
	[SerializeField] GameObject prefabButton; //button which is instatiated for dropdown
	[SerializeField]GameObject contentCanvas; //canvas is needed to 
	[SerializeField]UIController uiController; //script to call gotomenu funtion
	[SerializeField]GameObject stageObject; //to change text of stage
	private List<GameObject> InstantiatedDropdown = new List<GameObject>(); //list to keep track of all buttons to erase
	private List<string> nameOfUcilnice = new List<string>();
	private List<string> typeOfProstor = new List<string>(){"Ucilnica", "Kabinet", "Vsi prostori"};//all posibilities of type of rooms
	private List<string> stringOfStage = new List<string>()//all posibilities of text stages
	{
	"Izberi tip prostora",
	"Izberi pozicijo",
	};
	private string selectedTypeOfProstor; //selected type
	private string selectedPosition; //selected position
	//public Button tempButton = new Button(); //just to call funtion gotoMenu

	//Initialize all classroms
	void Awake(){
		ucilnice = ProstoriSole.ucilnice;
		//Debug.Log("HERE");
		//makes temporary copy so that doesnt come to duplicates
		List<string> _nameOfUcilnice = new List<string>();
		for(int i = 0; i < ucilnice.Count; ++i){
			_nameOfUcilnice.Add(ucilnice[i].ime_Ucilnice);
		}
		//copies temporary list
		nameOfUcilnice = _nameOfUcilnice;
		//sorting by name
		//ucilnice.Sort(delegate(Sola.Ucilnica elem1, Sola.Ucilnica elem2) { return elem1.ime_Ucilnice.CompareTo(elem2.ime_Ucilnice); });
		nameOfUcilnice.Sort();
	}

	/// <summary>
	/// Function that populate dropdown menu with prefabs 
	/// Case 1: Select which type of Prostor u want to see
	/// Case 2: Choose Prostor of this type
	/// Case 3: Save it
	/// </summary>
	public void PopulateDropDownMenu(string stageText, int stageOfProces){
		//Debug.Log(stageOfProces.ToString());
		switch (stageOfProces) {
			case 1:
				stageObject.GetComponentInChildren<Text>().text = stringOfStage[stageOfProces-1];
				//instantiate all options
				for(int i = 0; i < typeOfProstor.Count; ++i){
					CreateButton(typeOfProstor[i]);
				}
				//remember in which state we are
				PlayerPrefs.SetInt("stage", stageOfProces);
				break;
			case 2:
				stageObject.GetComponentInChildren<Text>().text = stringOfStage[stageOfProces-1];
				EraseButtons();
				selectedTypeOfProstor = stageText;
				if(selectedTypeOfProstor == "Vsi prostori"){
					for (int i =0; i<nameOfUcilnice.Count; ++i){
						CreateButton(nameOfUcilnice[i]);
					}
				}else{
					for(int i = 0; i < nameOfUcilnice.Count; ++i){
						//if name of ucilnice mathes pattern(selectedTypeOfProstor) print it out
						if (System.Text.RegularExpressions.Regex.IsMatch(nameOfUcilnice[i], selectedTypeOfProstor, System.Text.RegularExpressions.RegexOptions.IgnoreCase)){
							CreateButton(nameOfUcilnice[i]);
						}
					}
				}
				//remember in which state we are
				PlayerPrefs.SetInt("stage", stageOfProces);
				break;
			case 3:
				selectedPosition = stageText;
			  	if(PlayerPrefs.GetInt("WhereSave") == 1){
			  		PlayerPrefs.SetInt("begin", StringToIdUcilnice(selectedPosition));
			  		PlayerPrefs.SetString("zacText", selectedPosition);
			  	}
			  	if(PlayerPrefs.GetInt("WhereSave") == 2){
			  		PlayerPrefs.SetInt("target", StringToIdUcilnice(selectedPosition));
			  		PlayerPrefs.SetString("konText", selectedPosition);
			  	}
			  	EraseButtons();
				uiController.saveProstor();
				break;		
		}
	}

	public void EraseButtons(){
		for(int i=0; i < InstantiatedDropdown.Count; ++i){
			//imediatly destroys button
			Destroy(InstantiatedDropdown[i],0);
		}
	}

	void CreateButton(string displayText){
		GameObject insButton = Instantiate(prefabButton) as GameObject;
		//set paretn to our canvas so that is perfectly lined with button
		insButton.transform.SetParent(contentCanvas.transform, false);
		//change text to know which type of Room represents
		insButton.GetComponentInChildren<Text>().text = displayText;
		//store buttons so that i can erase them later
		InstantiatedDropdown.Add(insButton);
	}

	/// <summary>
	/// Strings to identifier ucilnice. This function gets name of classroms and return id of classrom
	/// </summary>
	/// <returns>The to identifier ucilnice.</returns>
	/// <param name="imeUcilnice">IME ucilnice.</param>
	public int StringToIdUcilnice(string imeUcilnice){
		int id = -1;
		for (int i=0; i<ucilnice.Count; ++i) {
			if(ucilnice[i].ime_Ucilnice == imeUcilnice){
				id = i;
				break;
			}
		}
		return id;
	}
}


