﻿//Author: Rok Kos
//Date: 26.12.2015
//Description: Script for free roaming and controling character

using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

//thanks to CnControls package
using CnControls;

public class FreeRoam : MonoBehaviour {

	private GameObject mainCamera; //camera for rotating
	private float speedOfJoystick = 0.15f; //determing how fast is movement
	private float speedOfRotation= 1.1f; //determing how fast is roatiton

	/// <summary>
	/// gets camera for rotating
	/// </summary>
	void Start(){
		mainCamera = GameObject.Find("Main Camera");
		//sets rotation in direction whic player is moving
		mainCamera.transform.rotation = new Quaternion (transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
		mainCamera.transform.Rotate (12.0f, 0, 0, Space.Self);
	}

	/// <summary>
	/// On update gets input from two joysticks
	/// </summary>
	void Update () {
		/*
		//CnControls -> gets input from joystick
		float xCnInput = CnInputManager.GetAxis ("Vertical");//gets vertical input
		float yCnInput = CnInputManager.GetAxis ("Horizontal");//gets horizontal input
		//changes position relative to rotation(because of transform.forward)
		transform.position += transform.forward * xCnInput * speedOfJoystick;
		//rotates around self
		transform.Rotate (0, yCnInput, 0, Space.Self);
		*/

		//Getting all inputs
		//first 2 are for moving
		//second 2 are for rotating camera
		float MoveX = CrossPlatformInputManager.GetAxisRaw("HorizontalMove");
		float MoveY = CrossPlatformInputManager.GetAxisRaw("VerticalMove");
		float LookX = CnInputManager.GetAxisRaw("Horizontal");
		float LookY = CnInputManager.GetAxisRaw("Vertical");
		//Debug.Log("Horizontal: " + CrossPlatformInputManager.GetAxisRaw("HorizontalLook").ToString() + " Vertical: " + CrossPlatformInputManager.GetAxisRaw("VerticalLook"));
		//moving player in that direction
		transform.position += transform.forward * MoveY * speedOfJoystick;
		transform.position += transform.right * MoveX * speedOfJoystick;
		//transform.position += new Vector3(MoveY * Time.deltaTime * speedOfJoystick, 0f, -1 * MoveX * Time.deltaTime * speedOfJoystick);
		//first rotate around your x axis
		//only camera because player cant!!!!
	 	mainCamera.transform.Rotate(new Vector3(-LookY * speedOfRotation, 0, 0), Space.Self);// 
		//and then around global y axis which is independant from object of rotating
		transform.Rotate(new Vector3(0,LookX * speedOfRotation, 0), Space.World);
		//and also rotate camera to se where are you going
		mainCamera.transform.Rotate(new Vector3(0,LookX * speedOfRotation, 0), Space.World);
		//alowing player to see only 35 degres up and down
		if (mainCamera.transform.localEulerAngles.x < 325 && mainCamera.transform.localEulerAngles.x > 180) {
			//seting camera back to 35 degres
			mainCamera.transform.Rotate(325 - mainCamera.transform.localEulerAngles.x, 0, 0, Space.Self);
		}
		if (mainCamera.transform.localEulerAngles.x > 35 && mainCamera.transform.localEulerAngles.x < 180){
			mainCamera.transform.Rotate(35 - mainCamera.transform.localEulerAngles.x, 0, 0, Space.Self);
		}

	}
}
