﻿//Author: Rok Kos
//Date: 12.12.2015
//Description: Script for testing and debuging 

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using GimVic;

public class TestingScript : MonoBehaviour {
	private int visina, sirina, kol;
	//boolean vairable that indicates if dropdown is shown
	private bool odprt_zacetek = false,
	odprt_konec = false;
	// string to represent what to output ot user
	private string izbran_text_zacetek = "Izberi zacetno pozicijo",
	izbran_text_konec = "Izberi koncno pozicijo";
	//varialbe that is passed to navmesh for start and end location
	private int izbran_start_id = -1,
	izbran_konec_id = -1;
	private List<Sola.Ucilnica> ucilnice;//array of objects which represent all classroms
	private ControllerScript controller;
	[SerializeField] Premikanje premikanje;
	private bool zeizbran = true;
	
	void Awake(){
		ucilnice = ProstoriSole.ucilnice;
		controller = GetComponent<ControllerScript> ();
	}

	// Update is called once per frame
	void Update () {
		//Gets height and width of screen
		visina = Screen.height;
		sirina = Screen.width;
		//Debug.Log (visina + " " + sirina);
		if (izbran_konec_id != -1 &&
		    izbran_start_id != -1 &&
		    izbran_konec_id != izbran_start_id &&
		    zeizbran) {
			PlayerPrefs.SetInt("begin", izbran_start_id);
			PlayerPrefs.SetInt("target", izbran_konec_id);
			controller.App_Play();
			premikanje.SetAgent();
			zeizbran = false;
		}
	}


	void OnGUI(){
		//racunamje za prileganje gumbov zaslonu
		int but1_x = sirina / 4;
		int but1_y = visina / 4;
		int but1_ratio_x = 100 * sirina / visina;
		int but1_ratio_y = 50 * visina/sirina;

		if(GUI.Button(new Rect( but1_x,but1_y,but1_ratio_x,but1_ratio_y), izbran_text_zacetek)){
			odprt_zacetek = true;
		}
		
		if (odprt_zacetek) {
			for(int i=0; i<ucilnice.Count; ++i){
				string trenutno_ime_ucilnice = ucilnice[i].vrni_ime();
				if(GUI.Button(new Rect(but1_x,but1_y + (but1_ratio_y * (i+1)),but1_ratio_x,but1_ratio_y), trenutno_ime_ucilnice)){
					izbran_text_zacetek = trenutno_ime_ucilnice;
					izbran_start_id = i;
					odprt_zacetek = false;
				}
			}
		}


		if(GUI.Button(new Rect(but1_x + 1.5f * but1_ratio_x, but1_y,but1_ratio_x,but1_ratio_y), izbran_text_konec)){
			odprt_konec = true;
		}

		if (odprt_konec) {
			for(int i=0; i<ucilnice.Count; ++i){
				string trenutno_ime_ucilnice = ucilnice[i].vrni_ime();
				if(GUI.Button(new Rect(but1_x + 1.5f * but1_ratio_x, but1_y+ (but1_ratio_y * (i+1)),but1_ratio_x,but1_ratio_y), trenutno_ime_ucilnice)){
					izbran_text_konec = trenutno_ime_ucilnice;
					izbran_konec_id = i;
					odprt_konec = false;
				}
			}
		}
		/*
		float x_coridnates = testbut.transform.position.x;
		float y_coridnates = testbut.transform.position.y;
		RectTransform testbut_rect = testbut.GetComponent<RectTransform> ();
		float x_size = testbut_rect.rect.width;
		float y_size = testbut_rect.rect.height;
		Debug.Log (x_coridnates.ToString ()+ " " + y_coridnates.ToString () + " " + x_size.ToString () + " " + y_size.ToString ());
		for(int i=0; i<ucilnice.Count; ++i){
			string trenutno_ime_ucilnice = ucilnice[i].vrni_ime();
			if(GUI.Button(new Rect(x_coridnates,y_coridnates+ (y_size * (i+1)),x_size,y_size), trenutno_ime_ucilnice)){
				izbran_text_konec = trenutno_ime_ucilnice;
				izbran_konec_id = i;
				odprt_konec = false;
			}
		}
	*/
	}

}
