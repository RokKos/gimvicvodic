﻿//Author: Rok Kos
//Date: 12.12.2015
//Description: Script for testing and debuging 

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CallButton : MonoBehaviour {
	private IzberiDestinacijo izberiDestinacijo;

	/// <summary>
	/// Finds the UIControler script
	/// </summary>
	void Start () {
		izberiDestinacijo = GameObject.Find ("Controller").GetComponent<IzberiDestinacijo> ();
	}
	/// <summary>
	/// Calls the function HideDropdown.
	/// </summary>
	public void CallFunction(){
		//find text and tag and send to Pokazi moznosti
		string sendText = transform.GetComponentInChildren<Text> ().text;
		int sendStage = PlayerPrefs.GetInt("stage") + 1;
		izberiDestinacijo.PopulateDropDownMenu (sendText, sendStage);
	}
}
