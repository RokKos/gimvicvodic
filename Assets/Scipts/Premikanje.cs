﻿//Author: Rok Kos
//Date: 05.12.2015
//Description: Script for moving user(camera) arround with navmesh

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

using GimVic;//where are all classroms defined

public class Premikanje : MonoBehaviour {
	NavMeshAgent agent; //agent which navigates or user
	int end_Target = 0;//where to go
	int begin_Target = 0; //where to start
	private bool zacetek = true;
	private List<Vector3> target = new List<Vector3>(); //targets where to go
	float dolzina_poti = 0.0f; //path distance
	int spawnDistance = 15;// distance to spawn object arrow
	float opravljenaPot = 0.0f; //distance allready done
	float beforePath = 0.0f; //distance allready done
	[SerializeField] GameObject arrow;

	//initialize and get navmesh agent and also the targets
	void Awake () {
		agent = GetComponent<NavMeshAgent> ();
		target = ProstoriSole.target; //initalize all the targets
	}
	
	void Update () {
		if (!zacetek) {
			//calculates route to destination
			dolzina_poti = Vector3.Distance (transform.position, target [end_Target]);
			//Debug.Log (dolzina_poti);
			opravljenaPot+= Math.Abs(beforePath-dolzina_poti);
			beforePath = dolzina_poti;
			//Debug.Log(opravljenaPot.ToString());
			/*if(opravljenaPot>=spawnDistance ){
				GameObject _arrow =Instantiate(arrow, transform.position + transform.forward * 5, transform.rotation) as GameObject;
				_arrow.transform.Rotate(90,0,0,Space.Self);
				_arrow.transform.Rotate(0,90,0,Space.Self);
				opravljenaPot = 0.0f;

			}*/
			//check if its alredy got to destination
			if (dolzina_poti != Mathf.Infinity && dolzina_poti <= 2.15f) {
				//Debug.Log ("tukaj");
				//stops the navigation
				GameObject.Find("Controller").GetComponent<ControllerScript>().App_Stop();
				zacetek = true;
			}
		}
	}

	/// <summary>
	/// Sets the agent to final destination.
	/// </summary>
	public void SetAgent(){
		//gets end and start target
		end_Target = PlayerPrefs.GetInt ("target", 0);
		begin_Target = PlayerPrefs.GetInt ("begin",0);
		//Debug.Log(target [begin_Target].ToString());
		//Instantiate(arrow, target[begin_Target], Quaternion.identity);
		//transform.position = (target [begin_Target]);
		//sets destination
		agent.SetDestination (target [end_Target]);
		//calculates distance to destination
		dolzina_poti =  Vector3.Distance (target [begin_Target], target [end_Target]);
		beforePath = dolzina_poti; //to calculate opraveljenoPot
		//Debug.Log (dolzina_poti);
		zacetek = false;
	}

}
