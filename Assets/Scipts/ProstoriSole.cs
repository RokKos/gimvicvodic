﻿//Author: Rok Kos
//Date: 07.12.2015
//Description: Script where all classroms are saved

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace GimVic{

	public class ProstoriSole : MonoBehaviour {
		//array of objects which represent all classroms(files must bi static so that other classes can get to them)static 
		public static  List<Sola.Ucilnica> ucilnice = new List<Sola.Ucilnica>(); 
		public static List<Vector3> target = new List<Vector3>(); //array of targets which are used to navigate nav mesh
		//Initializing all classroms
		void Awake () {
			//Uncoment this line if you need new initialization of ListOfUcilnice.txt
			//exportToFile();
			readFile();
		}

		
		//creates a string of all classroms for representation or output
		string Skupni_izpis(List<Sola.Ucilnica> izpis){
			string tmp = "";
			for (int i=0; i<izpis.Count; i++) {
				Debug.Log(izpis[i].vrni_class());
				tmp+=izpis[i].vrni_class();
			}
			return tmp;
		}
		//fills target array with positions from classroms
		void naredi_pozicije(){
			for (int i=0; i<ucilnice.Count; ++i) {
				Vector3 trenutni_vector =ucilnice[i].vrni_pozicijo();
				target.Add(trenutni_vector);
			}
		}

		/// <summary>
		/// Funtion that export all elements in ucilnice to text file for later use
		/// </summary>
		void exportToFile(){
			//Gets all the classroms
			GameObject[] GetProstori = GameObject.FindGameObjectsWithTag("Prostor");
			for (int i=0; i<GetProstori.Length; ++i) {
				GameObject _Prostor = GetProstori[i];
				//Debug.Log(_Prostor.name);
				ucilnice.Add(new Sola.Ucilnica(_Prostor.transform.position,_Prostor.transform.rotation, _Prostor.name, i));
			}

			//open file in Streawriter and at the end automaticly closes file
			using (System.IO.StreamWriter file = 
            new System.IO.StreamWriter(@"D:\Documents\Unity\GimVicVodic\Assets\Resources\ListOfUcilnice.txt"))
	        {
	        	//goes throu every elemnt in ucilnice and writes in file
	            foreach (Sola.Ucilnica element in ucilnice){
	           		file.WriteLine(element.vrni_class());
	           		Debug.Log(element.vrni_class());
	            }
	        }			
		}
		/// <summary>
		/// Funtion that imports all elements from file to project
		/// </summary>
		void readFile(){
		//string[] readLines = System.IO.File.ReadAllLines(@"D:\Documents\Unity\GimVicVodic\Assets\ListOfUcilnice.txt");
		TextAsset _textAsset = Resources.Load("ListOfUcilnice") as TextAsset;
		string[] readLines = _textAsset.text.Split("\n"[0]);
		//temporary list so that doesnt come to duplicates when returning back to Main menu
		List<Sola.Ucilnica> _ucilnice = new List<Sola.Ucilnica>(); 
		for(int i=0; i< readLines.Length-1; ++i){
			//Debug.Log(element);
			//element -> ime_Ucilnice, pozicija.x, pozicija.y, pozicija.z, rotacija, NFC_tag;
			string[] podElementi = readLines[i].Split(';');
			string _ime_Ucilnice = podElementi[0];
			float _pozicijaX = Single.Parse(podElementi[1]);
			float _pozicijaY = Single.Parse(podElementi[2]);
			float _pozicijaZ = Single.Parse(podElementi[3]);
			int _NFC_tag = Int32.Parse(podElementi[5]);
			//TODO: add parsing of roation
			Sola.Ucilnica tempUcilnica = new Sola.Ucilnica(new Vector3(_pozicijaX, _pozicijaY, _pozicijaZ), _ime_Ucilnice, _NFC_tag);
			_ucilnice.Add(tempUcilnica);
		}
		ucilnice = _ucilnice;
		naredi_pozicije ();
	}
	}
}
