﻿//Author: Rok Kos
//Date: 05.12.2015
//Description: Script for camera to follow user(giving first person view)

using UnityEngine;
using System.Collections;

public class Camera_Premikanje : MonoBehaviour {

	Transform premik; //our user position
	[SerializeField] Camera cam; //our camera
	[SerializeField] ControllerScript controllerScript;
	//initaliza camera to folow object
	void Awake(){
		GameObject tempClovek = GameObject.Find("Clovek");
		premik = tempClovek.transform;
	}

	//Camera follows object
	void Update () {
		cam.transform.position = new Vector3 (premik.position.x, premik.position.y+1, premik.position.z); 
		//if player is in navigation mode camera rotation is same as player
		//else it is rotatet by joystick
		if(PlayerPrefs.GetInt("mode",1) == 1){
			cam.transform.rotation = new Quaternion (premik.rotation.x, premik.rotation.y, premik.rotation.z, premik.rotation.w);
			cam.transform.Rotate (12.0f, 0, 0, Space.Self);
		}
	}
}
